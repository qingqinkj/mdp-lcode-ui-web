/* Layout */
import { Layout } from '@/utils/routerHelper'
const { t } = useI18n()

const RoutesOrder: AppRouteRecordRaw[] = [
  {
    path: '/my/order',
    component: Layout,
    name: 'moOrder',
    meta: {
      title: '订单管理',
      icon: 'simple-line-icons:note'
    },
    children: [
      {
        path: 'branch/list',
        component: () => import('@/views/mdp/menu/menuModuleBranch/Index.vue'),
        name: 'moBranchList',
        meta: { title: t('router.order.MenuModuleBranchMng'), icon: 'component', menu: false,roles:['superAdmin']}
      },

      {
        path: 'platfrom/list',
        component: () => import('@/views/mdp/mo/moOrder/Index.vue'),
        name: 'moPlatformList',
        meta: {
          title: t('router.order.MoOrderMng'),
          icon: 'component',
          menu: true
        }
      },
      {
        path: 'my/list',
        component: () => import('@/views/mdp/mo/moOrder/MyMoOrderMng.vue'),
        name: 'moMyList',
        meta: {
          title: t('router.order.MyMoOrderMng'),
          icon: 'component',
          menu: true
        }
      },
      {
        path: 'index',
        component: () => import('@/views/order/index.vue'),
        name: 'moIndex',
        meta: {
          title: t('router.order.MyPurchaseMng'),
          icon: 'component',
          menu: true
        }
      },
      {
        path: 'create',
        component: () => import('@/views/order/createOrder.vue'),
        name: 'moCreateOrder',
        meta: {
          title: t('router.order.CreateOrder'),
          icon: 'component',
          menu: true,
          hidden: true
        }
      },
      {
        path: 'addUsers',
        component: () => import('@/views/order/addUsersIndex.vue'),
        name: 'moAddUsersIndex',
        meta: {
          title: t('router.order.AddUsersIndex'),
          icon: 'component',
          menu: true
        }
      },
      {
        path: 'renew',
        component: () => import('@/views/order/renewIndex.vue'),
        name: 'moRenewIndex',
        meta: {
          title: t('router.order.RenewIndex'),
          icon: 'component',
          menu: true
        }
      },
      {
        path: 'paySuccess',
        component: () => import('@/views/order/paySuccess.vue'),
        name: 'paySuccess',
        meta: {
          title: '支付成功',
          icon: 'component',
          hidden: true,
          menu: true
        }
      },
      {
        path: 'alipay',
        component: () => import('@/views/order/alipay.vue'),
        name: 'alipay',
        meta: {
          title: '支付宝支付码页面',
          icon: 'component',
          hidden: true
        }
      }
    ]
  }
]

export default RoutesOrder
