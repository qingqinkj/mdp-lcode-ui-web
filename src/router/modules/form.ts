/* Layout */
import { Layout } from '@/utils/routerHelper'
const { t } = useI18n()

const RoutesForm: AppRouteRecordRaw[] = [
  {
    path: '/mdp/form',
    component: Layout,
    name: 'AiForm', 
    meta: {
      title: t('router.form.IntelligentForm'),
      icon: 'simple-line-icons:screen-tablet'
    },
    //leaf: true,//只有一个节点
    children: [
      { 
        path: 'index',
        component: () => import('@/views/mdp/form/formDef/Index.vue'),
        name: 'AiFormCenter',
        meta: {
          title: t('router.form.FormDefMng'), 
          menu: true
        }
      },
      {
        path: 'design/detail/:expandId',
        component: () => import('@/views/mdp/form/formDef/DesignRoute.vue'),
        name: 'AiFormDesignDetail',
        meta: {
          title: t('router.form.FormDetail'),   
          hidden:true,
          menu: true
        }
      }, 
      {
        path: 'design/edit/:expandId',
        component: () => import('@/views/mdp/form/formDef/DesignRoute.vue'),
        name: 'AiFormDesignEdit',
        meta: {
          title: t('router.form.FormEdit'),   
          hidden:true,
          menu: true
        }
      }, 
      { 
        path: 'design/add',
        component: () => import('@/views/mdp/form/formDef/DesignRoute.vue'),
        name: 'AiFormDesigner',
        meta: { title: t('router.form.FormDesign'),
           menu: true
         }
      },   
      {
        path: 'data/add/:formId',
        component: () => import('@/views/mdp/form/formData/FormRoute.vue'),
        name: 'AiFormDataAddRoute',
        meta: { title: t('router.form.FormDataAddRoute'), hidden: true,
        menu: true }
      }
    ]
  }
]

export default RoutesForm
