import axios from '@/config/maxios'

 import config from '@/api/mdp_pub/mdp_config'

let base = config.getDmCtx();
  

//查询表格数据列表
export const listTableData = params => { return axios.get(`${base}/mdp/dm/data/list`, {params:params}); }; 

//查询数据集数据列表
export const listByDataSetId = params => { return axios.get(`${base}/mdp/dm/data/listByDataSetId`, {params:params}); };

//预览sql语句的执行结果
export const previewSqlData = params => { return axios.get(`${base}/mdp/dm/data/previewSqlData`, {params:params}); };

//新增表格数据
export const addTableData = params => { return axios.post(`${base}/mdp/dm/data/add`, params); };

//删除表格数据
export const delTableData = params => { return axios.post(`${base}/mdp/dm/data/del`, params); };

//批量删除表格数据
export const batchDelTableData = params => { return axios.post(`${base}/mdp/dm/data/batchDel`, params); };

//批量修改表格数据
export const editSomeFieldsTableData = params => { return axios.post(`${base}/mdp/dm/data/editSomeFields`, params); };

