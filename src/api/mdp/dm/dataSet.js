import axios from '@/config/maxios'

import config from '@/api/mdp_pub/mdp_config'

let base = config.getDmCtx();

/**
 * 数据集
 * 1 默认只开放普通查询，所有查询，只要上传	 分页参数 {pageNum:当前页码从1开始,pageSize:每页记录数,total:总记录【数如果是0后台会自动计算总记录数非0不会自动计算】}，后台都会自动按分页查询 其它 api用到再打开，没用到的api请注释掉，
 * 2 查询、新增、修改的参数格式  params={id:'数据集编号 主键',dsSql:'数据集sql',dataSource:'数据源-关联数据字典dm_data_source',dataModel:'数据模型-关联字典dm_data_model',title:'数据集标题',cuserid:'创建者',cbranchId:'创建者机构号',ctime:'创建时间',euserid:'最后修改人',etime:'最后修改时间',idTitleLinks:'字段编号与title对照；[{id:'字段编号',label:'字段说明'}]'}
 * @author maimeng-mdp code-gen
 * @since 2024-4-25
 **/
 
//普通查询 条件之间and关系  
export const listDataSet = params => { return axios.get(`${base}/mdp/dm/dataSet/list`, { params: params }); };

//普通查询 条件之间and关系
export const queryDataSetById = params => { return axios.get(`${base}/mdp/dm/dataSet/queryById`, { params: params }); };

//删除一条数据集 params={id:'数据集编号 主键'}
export const delDataSet = params => { return axios.post(`${base}/mdp/dm/dataSet/del`,params); };

//批量删除数据集  params=[{id:'数据集编号 主键'}]
export const batchAddDataSet = params => { return axios.post(`${base}/mdp/dm/dataSet/batchAdd`, params); };

//批量删除数据集  params=[{id:'数据集编号 主键'}]
export const batchDelDataSet = params => { return axios.post(`${base}/mdp/dm/dataSet/batchDel`, params); };

//修改一条数据集记录
export const editDataSet = params => { return axios.post(`${base}/mdp/dm/dataSet/edit`, params); };

//新增一条数据集
export const addDataSet = params => { return axios.post(`${base}/mdp/dm/dataSet/add`, params); };

//批量修改某些字段
export const editSomeFieldsDataSet = params => { return axios.post(`${base}/mdp/dm/dataSet/editSomeFields`, params); };