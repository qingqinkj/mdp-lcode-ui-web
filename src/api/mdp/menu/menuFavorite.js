import axios from '@/config/maxios'

import config from '@/api/mdp_pub/mdp_config'

let base = config.getMdpCtx();

/**
 * 菜单收藏夹
 * 1 默认只开放普通查询，所有查询，只要上传	 分页参数 {pageNum:当前页码从1开始,pageSize:每页记录数,total:总记录【数如果是0后台会自动计算总记录数非0不会自动计算】}，后台都会自动按分页查询 其它 api用到再打开，没用到的api请注释掉，
 * 2 查询、新增、修改的参数格式  params={id:'主键 主键',name:'菜单名称',sort:'排序',branchId:'云用户机构编号',cuserid:'创建人',cdate:'创建时间',accUrl:'访问路径,路由则填写路由的path',icon:'菜单图标',stype:'菜单类型1-路由，2-自定义',rname:'菜单类型为路由时，填写路由的name'}
 * @author maimeng-mdp code-gen
 * @since 2024-5-15
 **/
 
//普通查询 条件之间and关系  
export const listMenuFavorite = params => { return axios.get(`${base}/mdp/menu/menuFavorite/list`, { params: params }); };

//普通查询 条件之间and关系
export const queryMenuFavoriteById = params => { return axios.get(`${base}/mdp/menu/menuFavorite/queryById`, { params: params }); };

//删除一条菜单收藏夹 params={id:'主键 主键'}
export const delMenuFavorite = params => { return axios.post(`${base}/mdp/menu/menuFavorite/del`,params); };

//批量删除菜单收藏夹  params=[{id:'主键 主键'}]
export const batchAddMenuFavorite = params => { return axios.post(`${base}/mdp/menu/menuFavorite/batchAdd`, params); };

//批量删除菜单收藏夹  params=[{id:'主键 主键'}]
export const batchDelMenuFavorite = params => { return axios.post(`${base}/mdp/menu/menuFavorite/batchDel`, params); };

//修改一条菜单收藏夹记录
export const editMenuFavorite = params => { return axios.post(`${base}/mdp/menu/menuFavorite/edit`, params); };

//新增一条菜单收藏夹
export const addMenuFavorite = params => { return axios.post(`${base}/mdp/menu/menuFavorite/add`, params); };

//批量修改某些字段
export const editSomeFieldsMenuFavorite = params => { return axios.post(`${base}/mdp/menu/menuFavorite/editSomeFields`, params); };